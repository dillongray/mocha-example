var add = function(a, b) {
	return a + b;
}

var subtract = function(a, b) {
	return a - b;
}

var bar = function() {
	return "World";
}

var foo = function() {
	return "Hello " + this.bar();
}

var math = {
	add: add,
	subtract: subtract,
	foo: foo,
	bar: bar
}

module.exports = math;