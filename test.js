var chai = require('chai');
var sinon = require('sinon');
var math = require('./index.js');
var expect = chai.expect;

describe('(Library) Math', function() {
	describe('(Method) Add', function() {
		it('adds a and b', function() {
			expect(math.add(1, 2)).to.equal(3);
		});
	});

	describe('(Method) Subtract', function() {
		it('subtracts a from b', function() {
			expect(math.subtract(10, 5)).to.equal(5);
			expect(math.subtract(5, 10)).to.equal(-5);
		});
	});

	describe('Add and subtract', function() {
		it('adds an subtracts', function() {
			var result = math.subtract(math.add(5, 5), 7);
			expect(result).to.equal(3);
		});
	});

	describe('(Method) foo', function() {
		it('returns correct string', function() {
			var barStub = sinon.stub(math, 'bar').returns("Stubs!");
			var result = math.foo();
			expect(result).to.equal("Hello Stubs!");
			barStub.restore();
		});
	})
})