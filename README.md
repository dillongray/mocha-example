This is a simple example of testing with:
* [Mocha](https://mochajs.org/#getting-started) - Test suite
* [Chai](http://chaijs.com/guide/styles/) - Assertion Library
* [Sinon](http://sinonjs.org/releases/v4.4.2/) - Spy/Stub/Mock Library

# Installation
### Yarn
`yarn`
### NPM
`npm install`

# Usage
Run `npm test` to run the suite once
Run `npm test:watch` to run it in watch mode